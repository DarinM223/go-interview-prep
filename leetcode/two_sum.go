package leetcode

func TwoSum(nums []int, sum int) (int, int) {
	dict := make(map[int]int)
	for i, num := range nums {
		dict[num] = i
	}

	for idx1, num := range nums {
		if idx2, ok := dict[sum-num]; ok && idx1 != idx2 {
			return idx1, idx2
		}
	}
	return -1, -1
}
