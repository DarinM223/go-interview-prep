package leetcode

import "testing"

var twoSumTests = []struct {
	list []int
	sum  int
	idx1 int
	idx2 int
}{
	{
		[]int{1, 5, 3, 10, 11},
		8,
		1,
		2,
	},
	{
		[]int{1, 2, 3, 4},
		100,
		-1,
		-1,
	},
	{
		[]int{},
		1,
		-1,
		-1,
	},
	{
		[]int{3, 2, 4},
		6,
		1,
		2,
	},
}

func TestTwoSum(t *testing.T) {
	for _, test := range twoSumTests {
		idx1, idx2 := TwoSum(test.list, test.sum)
		if idx1 != test.idx1 || idx2 != test.idx2 {
			t.Errorf("Error: expected %d, %d got %d, %d", test.idx1, test.idx2, idx1, idx2)
		}
	}
}
